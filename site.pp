node 'puppetclient2.ec2.internal' ,'puppetclient.ec2.internal'{
 
  #### Instalando Docker
  #include 'docker'
 
  class { 'docker':
    use_upstream_package_source => false,
  }

  # Cria uma rede Docker
  docker_network { 'lb-net':
    ensure => 'present',
    driver => 'bridge',
    subnet => '172.18.0.0/16',
  }
  
  # Cria o diretório /opt/nginx-lb
  file { '/opt/nginx-lb':
    ensure => directory,
  }

  file { '/opt/nginx-lb/nginx.conf':
    ensure  => present,
    content => @("END"/L)
      events {
        worker_connections 1024;
      }

      http {
        upstream node {
          server node1:80;
          server node2:80;
        }

        server {
          listen 80;
          location / {
            proxy_pass http://node;
          }
        }
      }
      END
    ,
    require => File['/opt/nginx-lb'],
  }

  # node 1
  docker::run { 'node1':
    image   => 'nginx:latest',
    net     => 'lb-net', 
    hostname => 'node1',
    ports   => ['8081:80'],
    volumes => ['/opt/node1:/usr/share/nginx/html'],
    restart => 'always',
    require => [Docker_network['lb-net'],Class['docker']],
  }

  vcsrepo { '/opt/node1':
    ensure   => present,
    provider => git,
    source   => 'https://github.com/wilglison/pagina-teste.git',
    revision => 'main',
    require  => Docker::Run['node1'],
  }

  # node 2
  docker::run { 'node2':
    image   => 'nginx:latest',
    net     => 'lb-net',
    hostname => 'node2',
    ports   => ['8082:80'],
    volumes => ['/opt/node2:/usr/share/nginx/html'],
    restart => 'always',
    require => [Docker_network['lb-net'],Class['docker']],
  }

  vcsrepo { '/opt/node2':
    ensure   => present,
    provider => git,
    source   => 'https://github.com/wilglison/pagina-teste-azul.git',
    revision => 'main',
    require  => Docker::Run['node2'],
  }

  # Nginx Load Balancer
  docker::run { 'nginx-lb':
    image     => 'nginx:latest',
    ports     => ['80:80'],
    net       => 'lb-net',
    volumes   => ['/opt/nginx-lb/nginx.conf:/etc/nginx/nginx.conf'],
    restart   => 'always',
    require   => [Class['docker'], File['/opt/nginx-lb/nginx.conf'],Docker_network['lb-net'],Docker::Run['node1'],Docker::Run['node2']],
  }
}
